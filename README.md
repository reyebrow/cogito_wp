# Cogito: Foundation 5 Responsive theme for WP


## Quickstart:

### Prerequisites:

* Node. (see below for instructions)
* [Livereload](http://livereload.com) (you just need the browser extension)
* [grunt](http://gruntjs.com/) (you need to installl it globally on your system) `npm install -g grunt`

### Install Cogito

We're assuming that you have a site with a git repo at the root and a `public_html` folder inside it which contains your wordpress install. Adjust the commands below if this is not the case.

#### from the root of the site:

```
git submodule add git@bitbucket.org:reyebrow/cogito_wp.git public_html/wp-content/themes/cogito
```

Now we need to get things set up to use [grunt](http://gruntjs.com/), [npm](http://blog.nodejs.org/2011/03/23/npm-1-0-global-vs-local-installation/) and [bower](http://bower.io/)

```
$ cd public_html/wp-content/themes
$ cp -fr cogito/STARTER_KIT mythemename
$ cd mythemename

$ npm install
$ bower install

$ grunt
```

That last command will set up all your css files, js files etc.

#### Don't forget to add things back into git

```
git add .gitmodules
git add public_html/wp-content/themes
git commit -m "New cogito theme"
```

### Developing using Cogito

```
grunt dev
```

This will give you livereload server you can use to develop your site without reloading. Make sure to click the box on the browser plugin.




## Detailed setup instructions


## Getting set for development

### Installing your modules using `npm`

NPM is a node package manager that we will use to install all of our grunt components and bower.

If you don't already have npm installed you'll need to do so:

    $ curl http://npmjs.org/install.sh | sh

Then you can run the installer to get all the files we need to build. The next command reads the `package.json` file and installs all the required npm modules:

    $ npm install


### Setting up bower

[Bower](http://bower.io/) is a package manager and it is what we will be using to install foundation.

    $ npm install -g bower

Now we should have everything installed time to grab the necessary bower modules. Like `npm install` the next command will read the `bower.json` file and install all necessary bower modules.

    $ bower install

## Using with grunt:

[Grunt](http://gruntjs.com/) is a task runner which can build your js and minify things for your.

As of now there are three tasks:

* `grunt start`: Copy all the js you need and uglify it.
* `grunt build`: Concatenate all your js into `script.js`
* `grunt dev`: This task will continuously monitor and build your SASS as well as giving you a livereload server.
* `grunt` run the previous tasks in order: `start` -> `build` -> `dev`


## Resources:

* Template hierarchy: [http://codex.wordpress.org/Template_Hierarchy](http://codex.wordpress.org/Template_Hierarchy)
* The most useful WordPress graphic ever created: [http://codex.wordpress.org/images/1/18/Template_Hierarchy.png](http://codex.wordpress.org/images/1/18/Template_Hierarchy.png)

