=== Plugin Name ===
Contributors: Matt Reimer
Tags: foundation 4, responsive
Requires at least: 3.2.1
Tested up to: 3.3
Stable tag: 4.3


RESOURCES:

More about child themes: http://codex.wordpress.org/Child_Themes

Template hierarchy: http://codex.wordpress.org/Template_Hierarchy

The most useful WordPress graphic ever created: http://codex.wordpress.org/images/1/18/Template_Hierarchy.png

