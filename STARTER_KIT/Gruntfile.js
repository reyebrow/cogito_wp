module.exports = function(grunt) {

  grunt.initConfig({

    pkg: grunt.file.readJSON('package.json'),

    // Uglify our Javascript
    // ********************************************************************************************************
    uglify: {
      bowerlibs: {
        files: [ { src: 'js/vendor/modernizr.js', dest:'js/vendor/modernizr.js' } ]
      }
    },

    // Minify our images
    // ********************************************************************************************************
    imagemin: { 
      dynamic: {                        
        files: [{
          expand: true,                 
          cwd: 'img/',             
          src: '**/*.{png,gif}',  
          dest: 'img/'             
        }]
      }
    },

    // Copy Files out of Bower
    // ********************************************************************************************************
    copy: {
      js: {
        files: [
          { expand:true, cwd:'bower_components/foundation/js/', src:'foundation.min.js', dest: 'js/vendor/' },
          { expand:true, cwd:'bower_components/jquery/',    src:'jquery.min.js', dest: 'js/vendor/' },
          { expand:true, cwd:'bower_components/modernizr/', src:'modernizr.js', dest: 'js/vendor/' }
        ],
      },
      fonts: {
        files: [
          { expand:true, cwd:'bower_components/foundation-icon-fonts/', src:'foundation-icons.eot', dest: 'fonts/' },
          { expand:true, cwd:'bower_components/foundation-icon-fonts/', src:'foundation-icons.svg', dest: 'fonts/' },
          { expand:true, cwd:'bower_components/foundation-icon-fonts/', src:'foundation-icons.woff', dest: 'fonts/' },
          { expand:true, cwd:'bower_components/foundation-icon-fonts/', src:'foundation-icons.ttf', dest: 'fonts/' },
        ],
      },
    },
    
    // Execute Live Reload and code rebuild on file changes
    // ********************************************************************************************************
    watch: {
      css: {
        files: ['css/**.css'],
        options: { livereload: true}
      },
      js: {
        files: ['js/**/*.js'],
        tasks: ['concat']
      },
      liveJs: {
        files: ['script.js'],
        options: { livereload: true}
      },
      html: {
        files: ['**/*.html'],
        options: { livereload: true}
      },
      php: {
        files: ['**/*.php'],
        options: { livereload: true}
      },
    },

    // Run multiple processes at the same time
    // ********************************************************************************************************
    concurrent: {
        dev: ['watch', 'compass:dev'],
        options: { logConcurrentOutput: true }, 
    },

    // Compile SCSS
    // ********************************************************************************************************
   compass: {
      options: {
        watch: false,
        environment: 'production',
        outputStyle: 'nested',
        cssDir: 'css',
        sassDir: 'scss',
        imagesDir: 'images',
        javascriptsDir: 'js',
        fontsDir: 'fonts',
        httpPath: '/',
        relativeAssets: true,
        importPath: [
          'bower_components/foundation/scss',
          'bower_components/foundation-icon-fonts',
        ],
      },
      dist: {
        options: {
          watch: false,
          environment: 'production',
        },
      },
      // The Dev step includes a 'compass watch' directive.
      dev: {
        options: {
          watch: true,
          environment: 'development',
          outputStyle: 'nested',
        },
      },
    },

    // Combile files together
    // ********************************************************************************************************
    concat: {
        options: {
          separator: ';',
        },
        dist: {
          src: [
            'js/vendor/foundation.min.js', 
            'js/vendor/modernizr.js', 
            'js/vendor/jquery.styleSwitcher.js', 
            'js/app.js'
          ],
          dest: 'script.js',
        },
      },


  });

  // Load in all our  necessary grunt modules
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-concurrent');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-compass');

  // Define grunt tasks
  grunt.registerTask('default', ['start', 'build' ]);
  grunt.registerTask('start', ['copy:fonts', 'copy:js', 'uglify:bowerlibs']);
  grunt.registerTask('dev', [ 'concurrent:dev' ]);
  grunt.registerTask('build', [ 'imagemin', 'concat', 'compass:dist' ]);

};
