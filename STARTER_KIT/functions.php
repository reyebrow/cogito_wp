<?php

/*
Here's where you get to set up the widths of your columns. If you change
the number of columns you may need to redefine cogito_wp_get_cols in this
file

ALL SECTIONS MUST ADD UP TO total_columns (or not. maybe you like the look of a broken site)

*/
  $cogito_init = array(
    'total_columns'      => 12,
      //Three columns with right and left sidebar
    'three_columns_left'     => 3,
    'three_columns_content'  => 6,
    'three_columns_right'    => 3,
    
    //Two columns with right sidebar
    'two_columns_rsb_right'   => 3,
    'two_columns_rsb_content' => 9,
    
    //Two columns with Left sidebar
    'two_columns_lsb_left'    => 3,
    'two_columns_lsb_content' => 9,
    
    //1 Column. Centered by default
    'one_column_content'      => 12,

  /******************************************************************************************************
   *  Sets an array corresponding to the number and widths of your footers from left to right. 
   *  You can have as many footers as you want but the widths MUST add up to total_columns or Foundation columns 
   *  will hate you and stop answering your phone calls.
   * 
   *  Example:  3 columns of equal width  array(4,4,4);
                2 columns of widths 4 and 8 array(4,8);
   *****************************************************************************************************/

    'footers' => array( 4,5,3 )

  );  
  update_option( 'cogito_columns', $cogito_init );


/******************************************************************************************************
 *  EVERYTHING BELOW THIS POINT CAN BE DELETED. IT'S JUST FOR REFERENCE
 *****************************************************************************************************/


/**
 *  Add our custom images in here
 *
 */
// add_image_size( 'custom-image-size', 207, 239, true ); 



/**
 *  Create a new menu area:
 *
 *  you'll need to create the actual menu in wordpress back-end and assign it to this area.
 */
// function cogito_extra_menus(){
//   register_nav_menu( 'custom-nav', __( 'Custom Menu', 'cogito' ) );
// }



/**
 *  Create a new sidebar area:
 *
 *  You can also use unregister_sidebar($id) to remove ones you don't need
 */
// function cogito_extra_sidebars(){
//   register_sidebar(array(
//     'name' => __( 'My Custom Sidebar Area' ),
//     'id' => 'custom-sidebar',
//     'description' => __( 'Widgets in this area will be shown somewhere in the theme.' ),
//     'before_widget' => '<aside id="%1$s" class="widget %2$s">',
//     'after_widget' => "</aside>",
//     'before_title' => '<h3 class="widget-title"><span>',
//     'after_title' => '</span></h3>',
//   ));
// }





/**
 *  Unregister Widgets we haven't explicitly themed
 *  This function relies on a regular expression whitelist
 *
 */
 // function unregister_nonessential_widgets() {

 //  if ( empty ( $GLOBALS['wp_widget_factory'] ) )
 //      return;

 //  $widgets = $GLOBALS['wp_widget_factory']->widgets;

 //  // Widget Whitelist (regex)
 //  $allowed_widgets_patterns = array(
 //    '/WP_Widget_Text/',
 //    '/advanced_sidebar_menu_page/',
 //    '/wpfw_flickr_Widget/',
 //    '/Ai1ec_.*/',
 //    '/.*(MLA|NDP).*/'
 //  );

 //  foreach ($widgets as $class => $widget) {
 //    reset($allowed_widgets_patterns);
 //    $allow = false;

 //    while (!$allow && current($allowed_widgets_patterns)){
 //      $allow = preg_match(current($allowed_widgets_patterns), $class);
 //      next($allowed_widgets_patterns);
 //    }

 //    if (!$allow){
 //      unregister_widget($class);
 //    }
    
 //  }
 // }
 // add_action('widgets_init', 'unregister_nonessential_widgets', 11);







 /**
 *  Turn comments off on all new posts
 *
 */
// function default_comments_off( $data ) {
//     if( $data['post_status'] == 'auto-draft' ) {
//         $data['comment_status'] = 0;
//     } 
//     return $data;
// }
// add_filter( 'wp_insert_post_data', 'default_comments_off' );