;(function ($, window, undefined) {
  'use strict';

  var $doc = $(document),
      Modernizr = window.Modernizr;

  // Load in foundation after everything else so that 
  // Things like Top-bar will be sure to have markup
  // to work on.
  $(document).ready(function() {
      $(this).foundation();

      /**
       * Social media customizations
       **/
       var addthis_layers_config = {
          'theme' : 'gray',
          'responsive' : {
            'maxWidth' : '767px',
            'minWidth' : '0px'
          },

          'share' : {
            'title': 'Share',
            'position' : 'left',
            'numPreferredServices' : 5,
            'desktop': false,
            'mobile': true
          },

          'follow' : {
            'services' : [
              {'service': 'facebook', 'id': 'followname'},
              {'service': 'twitter', 'id': 'followname'},
              {'service': 'youtube', 'id': 'followname'},
              {'service': 'pinterest', 'id': 'followname'}
            ],
            'title' : 'Follow',
            'postFollowTitle' : 'Thanks for following!',
            'postFollowRecommendedMsg' : 'Recommended for you',
            'mobile' : true,
            'desktop' : false,
            'theme' : 'transparent'
          },

      };

      addthis.layers(addthis_layers_config);

      var addthis_config = {"data_track_addressbar":true};
      addthis.init();
  });

  // Hide address bar on mobile devices (except if 
  // #hash present, so we don't mess up deep linking).
  if (Modernizr.touch && !window.location.hash) {
    $(window).load(function () {
      setTimeout(function () {
        window.scrollTo(0, 1);
      }, 0);
    });
  }

})(jQuery, this);
