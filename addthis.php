<div class="addthis_toolbox addthis_custom">
  <span class="label-text fi-share">Share</span>
  
  <a class="twitter addthis_button_twitter at300b" title="Tweet" href="#">
    <i class="fi-social-twitter"></i>
    <span>Tweet</span>
  </a>
  <a class="facebook addthis_button_facebook at300b" title="Facebook" href="#">
    <i class="fi-social-facebook"></i>
    <span>Like</span>
  </a>
  <a class="pinterest addthis_button_pinterest_share at300b" target="_blank" title="Pinterest" href="#">
    <i class="fi-social-pinterest"></i>
    <span>Pin</span>
  </a>
  <a class="tumblr addthis_button_tumblr at300b" target="_blank" title="Tumblr" href="#">
    <i class="fi-social-tumblr"></i>
    <span>Tumblr</span>
  </a>
  <a class="addmore addthis_button_compact at300m" href="#">
    <i class="fi-plus"></i>
    <span>More</span>
  </a>

  <div class="atclear" tabindex="1000"></div>
</div>