<?php
/****************************************************************************************************************************/
/* WARNING!!!! NEVER CHANGE ANYTHING IN THIS FILE OR FOLDER!!! USE A CHILD THEME INSTEAD!                                   */
/* CHECK OUT "STARTER_CHILD" FOLDER FOR INSTRUCTIONS                                                                        */
/*****************************************************************************************************************************

Many of the functions in this file can be rewritten by creating functions with an identical name in a child theme
Just copy what's inside the 

              if ( ! function_exists()){
              
                    COPY THIS STUFF
              
              }

Then paste it into your child's functions.php and change away.
*/

add_theme_support( 'automatic-feed-links' ); // Add automatic feeds
add_theme_support( 'post-thumbnails' );    // Add post thumbnails
add_filter('widget_text', 'do_shortcode'); // Allow shortcodes in widgets 

// Note: the following is set here as a default. 
// You should really do this in your child theme. 

if (!isset($cogito_init) ){
  $cogito_init = array(
    // Set this to be the same as in _settings.scss. This is the size of your grid.
    'total_columns'      => 12,
    //Three columns with right and left sidebar
    'three_columns_left'     => 3,
    'three_columns_content'  => 6,
    'three_columns_right'    => 3,
    
    //Two columns with right sidebar
    'two_columns_rsb_right'   => 4,
    'two_columns_rsb_content' => 8,
    
    //Two columns with Left sidebar
    'two_columns_lsb_left'    => 3,
    'two_columns_lsb_content' => 9,
    
    //1 Column. Centered by default
    'one_column_content'      => 10,
    
    'footers' => array( 4,3,5 )
  ); 
}
update_option( 'cogito_init', $cogito_init );

// Look in the following files for more cogito goodness.
include_once( 'inc/theme_actions.php' ); // Theme actions are super useful for injecting code.
include_once( 'inc/utils.php' );         // Useful utilities to do things like change excerpt length etc.
include_once( 'inc/foundation.php' );    // All the grid calculation and adding foundation classes to things, topbar, pagination etc.


/**
 * cogito_enqueue_styles()
 *
 * Load in the stylesheets we need.
 * This takes a lot of tweaking because of how
 * problematic wordpress is with conditional 
 * stylesheets.
 */

if ( ! function_exists( 'cogito_enqueue_styles' ) ) :
  function cogito_enqueue_styles() {

    // Add conditional stylesheets for lesser browsers
    $maxIE = 8; //the top version of IE that we make exceptions for

    wp_register_style( 'all', get_stylesheet_directory_uri() . '/css/app.css'  );
    // $GLOBALS['wp_styles']->add_data( 'allcss', 'conditional', 'gte IE 9');
    wp_enqueue_style( 'all');    

    // Now load each css file if and only if there is a file that exists for this version
    for ($i = 8; $i <= $maxIE; $i++){

      if( is_file( get_stylesheet_directory() . '/css/ie'.$i.'.css') ){
        wp_register_style( 'app-ie'.$i, get_stylesheet_directory_uri() . '/css/ie'.$i.'.css'  );
        $GLOBALS['wp_styles']->add_data( 'app-ie' . $i , 'conditional', 'IE '.$i );
        wp_enqueue_style( 'app-ie'.$i );
      }

    } 

  }
endif;
add_action( 'wp_enqueue_scripts', 'cogito_enqueue_styles' );


/**
 * cogito_admin_enqueue_scripts()
 *
 * Let's add all the necessary javascript and styles
 */
if ( ! function_exists( 'cogito_admin_enqueue_scripts' ) ) :
  function cogito_admin_enqueue_scripts( $hook_suffix ) {
    // Grunt gives us everything we need in a single file.
    // Script.js should include foundation, jquery, modernizr etc.
    wp_enqueue_script( 'foundation-app', get_stylesheet_directory_uri() . '/script.js', array('jquery'));
  
  }
endif;
add_action( 'wp_enqueue_scripts', 'cogito_admin_enqueue_scripts' );

// Adding the AddThis scripts to the parent functions
function addthis_scripts( $hook_suffix ) {
  wp_enqueue_script('addthis', '//s7.addthis.com/js/300/addthis_widget.js#async=1', array(), '3', true);
}
add_action( 'wp_enqueue_scripts', 'addthis_scripts' );

// Disable WordPress version reporting as a basic protection against automatic attacks
function remove_generators() { return ''; }
add_filter( 'the_generator','remove_generators' );


/**
 * cogito_menu_init()
 *
 * Register menu regions
 */
if ( ! function_exists( 'cogito_menu_init' ) ) :
  function cogito_menu_init() {
  
  	// This theme uses wp_nav_menu() in one location.
  	register_nav_menu( 'main-nav', __( 'Primary Menu', 'cogito' ) );
  	register_nav_menu( 'mobile-nav', __( 'Primary Menu (mobile)', 'cogito' ) );

    // The following lets us define a function in the child theme
    // That adds/changes or overwrites anything9 here.
    if ( function_exists( 'cogito_extra_menus' ) ){
     cogito_extra_menus();
    }
  }
  
endif;
add_action( 'after_setup_theme', 'cogito_menu_init' );


/**
 * Register our sidebars and widgetized areas. Also register the main menu as a dynamic menu
 *
 * Registering your own areas: you can either override this whole function in the child
 * Or simply create a function called cogito_extra_sidebars() in your child with the extras.
 *
 */
if ( ! function_exists( 'cogito_widgets_init' ) ) :

  function cogito_widgets_init() {
  	  
    $defaults = array(
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => "</aside>",
      'before_title' => '<h3 class="widget-title"><span>',
      'after_title' => '</span></h3>',
    );

    // Generic LEFT Sidebar
    register_sidebar( 
      array_merge( $defaults, array(
          'name' => __( 'Left Sidebar', 'cogito' ),
          'id' => 'sidebar-left',
      )) 
    );

    // Generic RIGHT Sidebar
    register_sidebar( 
      array_merge( $defaults, array(
          'name' => __( 'Right Sidebar', 'cogito' ),
          'id' => 'sidebar-right',
      )) 
    );
  	
  	//Dynamically gererate footer column widget regions
    $cogito_init = get_option( 'cogito_init' ); 
    $footers = isset($cogito_init) && isset($cogito_init['footers']) && is_array($cogito_init['footers']) ? $cogito_init['footers'] : array(4,4,4);
    
    if (is_array($footers) && !empty($footers)){
      for ($i = 1; $i<= sizeof($footers); $i++){
        register_sidebar(  array_merge( $defaults, array(
          'name' => __( 'Footer Area '. $i , 'mla_template' ),
          'id' => 'footer-'. $i,
          'description' => __( 'An optional widget area for your site footer', 'mla_template' ),
        ) ) );
    	}
    }
    
    // The following lets us define a function in the child theme
    // That adds/changes or overwrites anything here.
    if ( function_exists( 'cogito_extra_sidebars' ) ){
  	 cogito_extra_sidebars();
  	}
  }
endif;
add_action( 'widgets_init', 'cogito_widgets_init' );




