<?php


/**********************************************

  Responsive Videos
  
  Wrap the video in foundation's video thingy

***********************************************/
//
function cogito_video_wrap($html, $url, $args){
if (preg_match('/^http:\/\/(www\.)?youtube.com\/watch.*/i', $url) || 
    preg_match('/^http:\/\/youtu.be\/*/i', $url) ) {
    $html = "<div class='flex-video'>" . $html . "</div>";
  }
  return $html;
}
add_filter( 'embed_oembed_html', 'cogito_video_wrap', 10, 3);


/**********************************************

  Remove the margin bump for the WP Admin
  header menu

***********************************************/
add_action('get_header', 'my_filter_head');

  function my_filter_head() {
    remove_action('wp_head', '_admin_bar_bump_cb');
  }

/**********************************************

  Simple Pagination

***********************************************/
function cogito_simple_pagination($pagenum, $numpages=0)
{  
    if ($pagenum < 1) $pagenum  =1;
    
    $paged_formal = "pagenum="; 
  
    $otherfields="";
    foreach($_GET as $key => $getparam){
      if ($key != "pagenum"){
        $otherfields .="&$key=$getparam";
      }
    }

    if($numpages > 0){ ?>
    
        <p class="paginator">
        <?php
          if ($pagenum > 1) { ?>
            <a href="<?php echo '?'.$paged_formal . ($pagenum -1) . $otherfields; ?>">&lt;</a>
                            <?php }
        for($i=1;$i<=$numpages;$i++){?>
            <?php $selected = 'class="selected" style="color:red;"';?>

            <a href="<?php echo '?'.$paged_formal. $i . $otherfields; ?>" <?php ($pagenum==$i)? print $selected : null;?>><?php echo $i;?></a>
        <?php } ?>
       <?php if($pagenum < $numpages){?>
            <a href="<?php echo '?'.$paged_formal. ($pagenum +1) . $otherfields; ?>">&gt;</a>
        <?php } ?>
        </p>
    <?php } 
}





/**
 * This function returns the columns and widths to be populated as classes.
 * It is recommended that you have this in your child's functions.php file 
 * since this is what will let you change the column widths
 */
if ( ! function_exists( 'cogito_col_class' ) ) :
  function cogito_col_class($col = ''){
  
    $cogito_init = get_option('cogito_init'); 
    $val = false;
  
    $widget_list =  wp_get_sidebars_widgets();

    //Is there a left column?
    if ( isset($widget_list['sidebar-left']) && !empty($widget_list['sidebar-left']) ) {
      $left = true;
    }
    
    //Is there a right column?
    if ( isset($widget_list['sidebar-right']) && !empty($widget_list['sidebar-right']) ) {
      $right = true;  
     }

    //It's a 3-column layout with a left and right sidebar.
    if ( isset($left) && isset($right) ) {
      switch ($col) {
          case 'content': $val = 'large-' . $cogito_init['three_columns_content'] . " columns push-". $cogito_init['three_columns_left']; break;
          case 'left':    $val = 'large-' . $cogito_init['three_columns_left'] . " columns pull-" . $cogito_init['three_columns_content']; break;
          case 'right':   $val = 'large-' . $cogito_init['three_columns_right'] . " columns"; break;
      }
    }
    //It's a 2-column layout with a left sidebar.
    elseif ( isset($left) ){
      switch ($col) {
          case 'content': $val = 'large-'. $cogito_init['two_columns_lsb_content'] . " columns push-" . $cogito_init['two_columns_lsb_left']; break;
          case 'left':    $val = 'large-'. $cogito_init['two_columns_lsb_left'] . " columns pull-" . $cogito_init['two_columns_lsb_content']; break;
      }
    }
    //It's a 2-column layout with a right sidebar.
    elseif ( isset($right) ){
      switch ($col) {
          case 'content': $val = 'large-'. $cogito_init['two_columns_rsb_content'] . " columns"; break;
          case 'right':   $val = 'large-'. $cogito_init['two_columns_rsb_right'] . " columns"; break;
      }
    }
    //It's a 1-column layout.
    else {
      switch ($col) {
          case 'content': $val = 'large-'. $cogito_init['one_column_content'] . " centered columns"; break;
      }
    }

    return $val;
  }
endif;




/**
 * topbar_nav()
 *
 * This function outputs a foundation top-bar using the top_bar_walker class.
 */
/*
https://gist.github.com/awshout/3943026
*/
if ( ! function_exists( 'topbar_nav' ) ) :
  function topbar_nav($menu_name) {
      wp_nav_menu(array( 
          'container' => false,                     // remove nav container
          'container_class' => 'menu',              // class of container
          'menu' => '',                             // menu name
          'menu_class' => 'top-bar-menu left',      // adding custom nav class
          'theme_location' => $menu_name,           // where it's located in the theme
          'before' => '',                           // before each link <a> 
          'after' => '',                            // after each link </a>
          'link_before' => '',                      // before each link text
          'link_after' => '',                       // after each link text
          'depth' => 5,                             // limit the depth of the nav
          'fallback_cb' => false,                   // fallback function (see below)
          'walker' => new top_bar_walker()
    ));
  } // end left top bar
endif;


/**
 * Customize the output of menus for Foundation top bar classes
 * The walker helps to build the topbar markup from zurb:
 *
 * http://foundation.zurb.com/docs/components/topbar.html
 *
 * Thos code was "borrowed" from  https://gist.github.com/awshout/3943026
 *
 */
class top_bar_walker extends Walker_Nav_Menu {
 
    function display_element($element, &$children_elements, $max_depth, $depth=0, $args, &$output) {
        $element->has_children = !empty($children_elements[$element->ID]);
        $element->classes[] = ($element->current || $element->current_item_ancestor) ? 'active' : '';
        $element->classes[] = ($element->has_children) ? 'has-dropdown' : '';
    
        parent::display_element($element, $children_elements, $max_depth, $depth, $args, $output);
    }
  
    function start_el(&$output, $item, $depth = 0, $args = Array(), $id = 0) {
        $item_html = '';
        parent::start_el($item_html, $item, $depth, $args); 
    
        $output .= ($depth == 0) ? '<li class="divider"></li>' : '';
    
        $classes = empty($item->classes) ? array() : (array) $item->classes;  
    
        if(in_array('label', $classes)) {
            $output .= '<li class="divider"></li>';
            $item_html = preg_replace('/<a[^>]*>(.*)<\/a>/iU', '<label>$1</label>', $item_html);
        }
        
        if ( in_array('divider', $classes) ) {
          $item_html = preg_replace( '/<a[^>]*>( .* )<\/a>/iU', '', $item_html );
        }
    
        $output .= $item_html;
    }
  
    function start_lvl(&$output, $depth = 0, $args = array()) {
        $output .= "\n<ul class=\"sub-menu dropdown\">\n";
    }
    
} // end top bar walker





// Custom Pagination (borrowed from wp_foundation)
/**
 * Retrieve or display pagination code.
 *
 * The defaults for overwriting are:
 * 'page' - Default is null (int). The current page. This function will
 *      automatically determine the value.
 * 'pages' - Default is null (int). The total number of pages. This function will
 *      automatically determine the value.
 * 'range' - Default is 3 (int). The number of page links to show before and after
 *      the current page.
 * 'gap' - Default is 3 (int). The minimum number of pages before a gap is 
 *      replaced with ellipses (...).
 * 'anchor' - Default is 1 (int). The number of links to always show at beginning
 *      and end of pagination
 * 'before' - Default is '<div class="emm-paginate">' (string). The html or text 
 *      to add before the pagination links.
 * 'after' - Default is '</div>' (string). The html or text to add after the
 *      pagination links.
 * 'next_page' - Default is '__('&raquo;')' (string). The text to use for the 
 *      next page link.
 * 'previous_page' - Default is '__('&laquo')' (string). The text to use for the 
 *      previous page link.
 * 'echo' - Default is 1 (int). To return the code instead of echo'ing, set this
 *      to 0 (zero).
 *
 * @author Eric Martin <eric@ericmmartin.com>
 * @copyright Copyright (c) 2009, Eric Martin
 * @version 1.0
 *
 * @param array|string $args Optional. Override default arguments.
 * @return string HTML content, if not displaying.
 */
if ( ! function_exists( 'emm_paginate' ) ) :
  function emm_paginate($args = null) {
    $defaults = array(
      'page' => null, 'pages' => null, 
      'range' => 3, 'gap' => 3, 'anchor' => 1,
      'before' => '<div class="row cogito_paginate"><ul class="pagination">', 'after' => '</ul></div>',
      'title' => __('<li class="unavailable"></li>'),
      'nextpage' => __('&raquo;'), 'previouspage' => __('&laquo'),
      'echo' => 1
    );
  
    $r = wp_parse_args($args, $defaults);
    extract($r, EXTR_SKIP);
  
//    print_r($r);
  
    if (!$page && !$pages) {
      global $wp_query;
  
      $page = get_query_var('paged');
      $page = !empty($page) ? intval($page) : 1;
  
      $posts_per_page = get_query_var('posts_per_page');

      $posts_per_page = $posts_per_page > 0 ? $posts_per_page : 1;

      $pages = intval(ceil($wp_query->found_posts / $posts_per_page));
    }
    $output = "";
    if ($pages > 1) { 
      $output .= "$before<li>$title</li>";
      $ellipsis = "<li class='unavailable'>...</li>";
  
      if ($page > 1 && !empty($previouspage)) {
        $output .= "<li><a href='" . get_pagenum_link($page - 1) . "'>$previouspage</a></li>";
      }
      
      $min_links = $range * 2 + 1;
      $block_min = min($page - $range, $pages - $min_links);
      $block_high = max($page + $range, $min_links);
      $left_gap = (($block_min - $anchor - $gap) > 0) ? true : false;
      $right_gap = (($block_high + $anchor + $gap) < $pages) ? true : false;
  
      if ($left_gap && !$right_gap) {
        $output .= sprintf('%s%s%s', 
          emm_paginate_loop(1, $anchor), 
          $ellipsis, 
          emm_paginate_loop($block_min, $pages, $page)
        );
      }
      else if ($left_gap && $right_gap) {
        $output .= sprintf('%s%s%s%s%s', 
          emm_paginate_loop(1, $anchor), 
          $ellipsis, 
          emm_paginate_loop($block_min, $block_high, $page), 
          $ellipsis, 
          emm_paginate_loop(($pages - $anchor + 1), $pages)
        );
      }
      else if ($right_gap && !$left_gap) {
        $output .= sprintf('%s%s%s', 
          emm_paginate_loop(1, $block_high, $page),
          $ellipsis,
          emm_paginate_loop(($pages - $anchor + 1), $pages)
        );
      }
      else {
        $output .= emm_paginate_loop(1, $pages, $page);
      }
  
      if ($page < $pages && !empty($nextpage)) {
        $output .= "<li><a href='" . get_pagenum_link($page + 1) . "'>$nextpage</a></li>";
      }
  
      $output .= $after;
    }
  
    if ($echo) {
      echo $output;
    }
  
    return $output;
  }
endif;






/**
 * Helper function for pagination which builds the page links. (borrowed from wp_foundation)
 *
 * @access private
 *
 * @author Eric Martin <eric@ericmmartin.com>
 * @copyright Copyright (c) 2009, Eric Martin
 * @version 1.0
 *
 * @param int $start The first link page.
 * @param int $max The last link page.
 * @return int $page Optional, default is 0. The current page.
 */
function emm_paginate_loop($start, $max, $page = 0) {
  $output = "";
  for ($i = $start; $i <= $max; $i++) {
    $output .= ($page === intval($i)) 
      ? "<li class='current'><a href='#'>$i</a></li>" 
      : "<li><a href='" . get_pagenum_link($i) . "'>$i</a></li>";
  }
  return $output;
} 




/**
 * Get Only active Footers 
 *
 * Set the widths of footer regions to space equally. Maximum of 4 regions possible
 *
 */
 
function cogito_get_footers() {

  //Dynamically gererate footer column widget regions
  $cogito_init = get_option('cogito_init'); 
  $footers = isset($cogito_init) && isset($cogito_init['footers']) && is_array($cogito_init['footers']) ? $cogito_init['footers'] : array(4,4,4);

  $widget_list =  wp_get_sidebars_widgets();

  $width_sum = 0;
  $last_used = 0;
   
  //First count the widget areas we have and store active footers in an array
  $foot_counter = Array();
  for ($i=0;$i<=4;$i++){

    if ( isset($widget_list['footer-' . ($i+1)]) && !empty($widget_list['footer-' . ($i+1)] ) ){
      //If the footer widths array has something useful in it use it. Otherwise 4 is a good number
      $width = isset($footers[$i]) ? $footers[$i] : 4;
      $foot_counter[$i+1] = $footers[$i];
      $width_sum +=  $footers[$i];
      $last_used = $i+1;
    }
  }

  //If they don't add up to $cogito_init['total_columns'] then add space on the end
  if ($width_sum < $cogito_init['total_columns'] && $last_used > 0){
    $foot_counter[$last_used] = $cogito_init['total_columns'] - $width_sum + $foot_counter[$last_used];
  }
  
  //Now print a block array 
  if ( $last_used > 0 ){
    foreach ($foot_counter as $key=>$footer_width){
      print '<div id="footer-'.$key.'" class="columns large-'. $footer_width .'">'; 
      dynamic_sidebar( 'footer-' . $key ); 
      print '</div>';
    }
  } 
  
}  
