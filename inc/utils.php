<?php

/* Many of the functions in this file can be rewritten by creating functions with an identical name in a child theme
Just copy what's inside the 

              if ( ! function_exists()){
              
                    COPY THIS STUFF
              
              }

Then paste it into your child's functions.php and change away.
*/

/**
 * Debugger function to output an array (only if logged in and administrator)
 **/
function d($obj, $title=""){
  if ( current_user_can( 'manage_options' ) ) {
    print "<div class='wp-debugger' style='border: 1px solid black; padding: 2rem; margin: 2rem;'><h3>$title</h3><pre>" . htmlentities(print_r($obj,1), ENT_QUOTES, "UTF-8"). "</pre></div>";  
  }
}


if ( ! function_exists( 'cogito_posted_on' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 * Create your own twentyeleven_posted_on to override in a child theme
 *
 * @since Twenty Eleven 1.0
 */
function cogito_posted_on() {

  $data = array(
    'permalink' => esc_url( get_permalink() ),
    'time' => esc_attr( get_the_time() ),
    'date_year' => esc_attr( get_the_date( 'c' ) ),
    'date' => esc_attr( get_the_date() ),
    'author_url' => esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
    'all_posts_by' => esc_attr( sprintf( __( 'View all posts by %s', 'cogito' ), get_the_author() ) ),
    'author' => get_the_author()
  );
  
  $data['output'] = sprintf( __( '<span class="sep">Posted on </span><a href="%1$s" title="%2$s" rel="bookmark"><time class="entry-date" datetime="%3$s" pubdate>%4$s</time></a><span class="by-author"> <span class="sep"> by </span> <span class="author vcard"><a class="url fn n" href="%5$s" title="%6$s" rel="author">%7$s</a></span></span>', 'cogito' ),
    $data['permalink'],
    $data['time'],
    $data['date_year'],
    $data['date'],
    $data['author_url'],
    $data['all_posts_by'],
    $data['author']
  );
  
  $data = apply_filters('cogito_posted_on', $data );
  
  print $data['output'];
}
endif;



/**
 * Sets the post excerpt length to 40 words.
 *
 * To override this length in a child theme, remove the filter and add your own
 * function tied to the excerpt_length filter hook.
 */
if ( ! function_exists( 'cogito_excerpt_length' ) ) :
 
  function cogito_excerpt_length( $length ) {
    return 40;
  }

endif;
add_filter( 'excerpt_length', 'cogito_excerpt_length' );




/**
 * Adds two classes to the array of body classes. (borrowed from twenty_eleven)
 * The first is if the site has only had one author with published posts.
 * The second is if a singular post being displayed
 *
 * @since Twenty Eleven 1.0
 */
if ( ! function_exists( 'cogito_body_classes' ) ) :

  function cogito_body_classes( $classes ) {
  
    if ( function_exists('is_multi_author') && ! is_multi_author() ) {
      $classes[] = 'single-author';
    }
  
    if ( is_singular() && ! is_home() )
      $classes[] = 'singular';
  
    return $classes;
  }
  
endif;
add_filter( 'body_class', 'cogito_body_classes' );




/**
 * Find a file by first looking in the child theme then in the parent
 *
 * @param int $filepath should be relative to the theme directory and include a slash.
 *
 */
function cogito_get_file_uri(array $filepaths){
  
  $root_wp = getcwd();

  // Cache path to theme for duration of this function:
  $cogito_dir = get_template_directory();
  $cogito_uri = get_template_directory_uri();
  
  $child_dir = get_stylesheet_directory();
  $child_uri = get_stylesheet_directory_uri();

  foreach ($filepaths as $filepath){
    if ( is_file( $child_dir . $filepath) ){
      return $child_uri . $filepath;
    }
  }
  //Now do it all again only this time in the parent directory
  foreach ($filepaths as $filepath){
    if( is_file( $cogito_dir . $filepath) ){
      return $cogito_uri . $filepath;
    }
  }
  return false;

}





/**
 * Automatically get and place all the icons necesssary (like favicon and apple-touch icons etc).
 *
 */
function cogito_get_icons(){
  
  $favicon =  cogito_get_file_uri(array('/images/icons/favicon.ico')); 

  // Sizes referenced from icongen.com
  $sizes = array(57,60,72,114,120,144,152);

  // If the icon exists, include it.
  foreach ($sizes as $size) {
    $file = cogito_get_file_uri(array("/images/icons/apple-touch-icon-".$size."x".$size.".png"));
    print $file ? '<link rel="apple-touch-icon" href="'.$file.'">' : "";
  }
  print $favicon ? '<link rel="shortcut icon" type="image/vnd.microsoft.icon" href="' . $favicon .'">' : "";

}


/**
 * cogito_content_nav()
 *
 * Display navigation to next/previous pages when applicable (borrowed from TwentyEleven)
 */
if ( ! function_exists( 'cogito_content_nav' ) ) :

  function cogito_content_nav( $nav_id ) {
    global $wp_query;
  
    if ( $wp_query->max_num_pages > 1 ) : ?>
      <nav id="<?php echo $nav_id; ?>">
        <div class="nav-previous"><?php next_posts_link( __( '<span class="meta-nav">&larr;</span> Older posts', 'cogito' ) ); ?></div>
        <div class="nav-next"><?php previous_posts_link( __( 'Newer posts <span class="meta-nav">&rarr;</span>', 'cogito' ) ); ?></div>
      </nav><!-- #nav-above -->
    <?php endif;
  }
  
endif;

/**
 * Test for logo.png in the places we expect to find it.
 *
 *
 * @return string $logo path to logo if found. False if otherwise.
 */
function cogito_get_logo(){

  //TODO: work in uploading a logo in the admin interface.
 
  if ($logo =  cogito_get_file_uri( array('/images/logo.png', '/logo.png') )){
    return $logo;
  }
  else{
    return false;
  }
  
}





/**
 * Adds a pretty "Continue Reading" link to custom post excerpts. (borrowed from twenty_eleven)
 *
 * To override this link in a child theme, remove the filter and add your own
 * function tied to the get_the_excerpt filter hook.
 */
if ( ! function_exists( 'cogito_custom_excerpt_more' ) ) :

  function cogito_custom_excerpt_more( $output ) {
    if ( has_excerpt() && ! is_attachment() ) {
      $output .= cogito_continue_reading_link();
    }
    return $output;
  }
  
endif;
add_filter( 'get_the_excerpt', 'cogito_custom_excerpt_more' );



/**
 * Returns a "Continue Reading" link for excerpts
 */
if ( ! function_exists( 'cogito_continue_reading_link' ) ) :

  function cogito_continue_reading_link() {
    return ' <a href="'. esc_url( get_permalink() ) . '">' . __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'cogito' ) . '</a>';
  }
  
endif;


/**
 * Replaces "[...]" (appended to automatically generated excerpts) with an ellipsis and twentyeleven_continue_reading_link().
 *
 * To override this in a child theme, remove the filter and add your own
 * function tied to the excerpt_more filter hook.
 */
if ( ! function_exists( 'cogito_auto_excerpt_more' ) ) :

  function cogito_auto_excerpt_more( $more ) {
    return ' &hellip;' . cogito_continue_reading_link();
  }
  
endif;
add_filter( 'excerpt_more', 'cogito_auto_excerpt_more' );






/**
 * Template for comments and pingbacks. (borrowed from twenty_eleven)
 *
 * To override this walker in a child theme without modifying the comments template
 * simply create your own cogito_comment(), and that function will be used instead.
 *
 * Used as a callback by wp_list_comments() for displaying the comments.
 *
 * @since Twenty Eleven 1.0
 */
if ( ! function_exists( 'cogito_comment' ) ) :
  function cogito_comment( $comment, $args, $depth ) {
    $GLOBALS['comment'] = $comment;
    switch ( $comment->comment_type ) :
      case 'pingback' :
      case 'trackback' :
    ?>
    <li class="post pingback">
      <p><?php _e( 'Pingback:', 'cogito' ); ?> <?php comment_author_link(); ?><?php edit_comment_link( __( 'Edit', 'cogito' ), '<span class="edit-link">', '</span>' ); ?></p>
    <?php
        break;
      default :
    ?>
    <li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
      <article id="comment-<?php comment_ID(); ?>" class="comment">
      <?php cogito_action_comment_top(); ?>
        <footer class="comment-meta">
          <div class="comment-author vcard">
            <?php
              $avatar_size = 68;
              if ( '0' != $comment->comment_parent )
                $avatar_size = 39;
  
              echo get_avatar( $comment, $avatar_size );
  
              /* translators: 1: comment author, 2: date and time */
              printf( __( '%1$s on %2$s <span class="says">said:</span>', 'cogito' ),
                sprintf( '<span class="fn">%s</span>', get_comment_author_link() ),
                sprintf( '<a href="%1$s"><time pubdate datetime="%2$s">%3$s</time></a>',
                  esc_url( get_comment_link( $comment->comment_ID ) ),
                  get_comment_time( 'c' ),
                  /* translators: 1: date, 2: time */
                  sprintf( __( '%1$s at %2$s', 'cogito' ), get_comment_date(), get_comment_time() )
                )
              );
            ?>
  
            <?php edit_comment_link( __( 'Edit', 'cogito' ), '<span class="edit-link">', '</span>' ); ?>
          </div><!-- .comment-author .vcard -->
  
          <?php if ( $comment->comment_approved == '0' ) : ?>
            <em class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', 'cogito' ); ?></em>
            <br />
          <?php endif; ?>
  
        </footer>
  
        <div class="comment-content"><?php comment_text(); ?></div>
  
        <div class="reply">
          <?php comment_reply_link( array_merge( $args, array( 'reply_text' => __( 'Reply <span>&darr;</span>', 'cogito' ), 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
        </div><!-- .reply -->
        <?php cogito_action_comment_bottom(); ?>
      </article><!-- #comment-## -->
  
    <?php
        break;
    endswitch;
  }
endif; // ends check for twentyeleven_comment()
